#!/bin/bash

KB="atreus62"
DOWNLOADED_JSON=$HOME/Downloads/atreus62_layout_mine.json

echo 'Generating keymap.c'

if [[ -f $DOWNLOADED_JSON ]]
then
	echo 'Found layout file in downloads dir'
	mv $DOWNLOADED_JSON ./
fi

cp *_layout_mine.json ../qmk_firmware

cd ../qmk_firmware

qmk json2c *_layout_mine.json > ../$KB/keymap.c
cat ../$KB/combos.c >> ../$KB/keymap.c

rm -rf keyboards/atreus62/keymaps/kenzietandun

ln -s $(pwd)/../atreus62 keyboards/atreus62/keymaps/kenzietandun

qmk compile -kb atreus62 -km kenzietandun -e CONVERT_TO=promicro_rp2040

qmk flash -kb atreus62 -km kenzietandun -e CONVERT_TO=promicro_rp2040
