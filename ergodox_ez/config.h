#pragma once

// keys held longer than this (in ms) registers as HOLD instead of TAP
#define TAPPING_TERM 200

// Disable auto repeating keys
#define TAPPING_FORCE_HOLD

// Prevent mod keys from registering if held less than TAPPING TERM
#define IGNORE_MOD_TAP_INTERRUPT

// Allows media codes to properly register in macros and rotary encoder code
#define TAP_CODE_DELAY 10

#define DISABLE_LEADER
