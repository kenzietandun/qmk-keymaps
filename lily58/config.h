#pragma once

// keys held longer than this (in ms) registers as HOLD instead of TAP
#define TAPPING_TERM 120

// Disable auto repeating keys
#define TAPPING_FORCE_HOLD

// Allows media codes to properly register in macros and rotary encoder code
#define TAP_CODE_DELAY 10

#define MASTER_LEFT

#define DISABLE_LEADER

#define NO_ACTION_MACRO
#define NO_ACTION_FUNCTION
