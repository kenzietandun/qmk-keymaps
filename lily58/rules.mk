BOOTLOADER = qmk-dfu
OLED_ENABLE = no
ENCODER_ENABLE = no       # Enables the use of one or more encoders
COMBO_ENABLE = yes

RGBLIGHT_ENABLE = no # Enable keyboard RGB underglow
LEADER_ENABLE = no        # Enable the Leader Key feature
MOUSEKEY_ENABLE = no
WPM_ENABLE = no
COMMAND_ENABLE = no
UNICODE_ENABLE = no
AUDIO_ENABLE = no
EXTRAKEY_ENABLE = yes       # Audio control and System control

EXTRAFLAGS += -flto