#!/bin/bash

KB="lily58"
FILENAME="lily58_rev1_kenzietandun.json"
DOWNLOADED_JSON="$HOME/Downloads/$FILENAME"

echo 'Generating keymap.c'

if [[ -f $DOWNLOADED_JSON ]]
then
	echo 'Found layout file in downloads dir'
	mv $DOWNLOADED_JSON ./
fi

rm ../qmk_firmware/$FILENAME
cp $FILENAME ../qmk_firmware

cd ../qmk_firmware

qmk json2c $FILENAME > ../$KB/keymap.c
cat ../$KB/combos.c >> ../$KB/keymap.c

rm -rf keyboards/lily58/keymaps/kenzietandun

ln -s $(pwd)/../lily58 keyboards/lily58/keymaps/kenzietandun

qmk compile -kb lily58 -km kenzietandun

qmk flash -kb lily58 -km kenzietandun