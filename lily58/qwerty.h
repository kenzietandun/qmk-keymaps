
// QWERTY LAYER

// upper row

#define QWERTY_Q KC_Q
#define QWERTY_W KC_W
#define QWERTY_E KC_E
#define QWERTY_R KC_R
#define QWERTY_T KC_T

#define QWERTY_Y KC_Y
#define QWERTY_U KC_U
#define QWERTY_I KC_I
#define QWERTY_O KC_O
#define QWERTY_P KC_P

// home row
#define QWERTY_A KC_A
#define QWERTY_S LALT_T(KC_S)
#define QWERTY_D KC_D
#define QWERTY_F KC_F
#define QWERTY_G KC_G

#define QWERTY_H   KC_H
#define QWERTY_J   KC_J
#define QWERTY_K   KC_K
#define QWERTY_L   RALT_T(KC_L)
#define QWERTY_SEM KC_SCLN

// lower row
#define QWERTY_Z KC_Z
#define QWERTY_X KC_X
#define QWERTY_C KC_C
#define QWERTY_V KC_V
#define QWERTY_B KC_B

#define QWERTY_N KC_N
#define QWERTY_M KC_M
#define QWERTY_COMM KC_COMM
#define QWERTY_DOT KC_DOT
#define QWERTY_SLSH KC_SLSH
