#include QMK_KEYBOARD_H

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
	switch (keycode) {
		case LSFT_T(KC_Z):            
			return 120;
		default:
			return TAPPING_TERM;
	}
}
