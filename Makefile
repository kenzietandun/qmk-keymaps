USER = kenzietandun
KEYBOARDS = kyria lily58 ergodox_ez lumberjack

PATH_kyria = splitkb/kyria
PATH_lily58 = lily58
PATH_ergodox_ez = ergodox_ez
PATH_lumberjack = peej/lumberjack

all: $(KEYBOARDS)

.PHONY: $(KEYBOARDS)
$(KEYBOARDS):
	# init submodule
	git submodule update --init --recursive

	# cleanup old symlinks
	rm -rf qmk_firmware/keyboards/$(PATH_$@)/keymaps/$(USER)

	# add new symlinks
	ln -s $(shell pwd)/$@ qmk_firmware/keyboards/$(PATH_$@)/keymaps/$(USER)

	# run lint check
	cd qmk_firmware; qmk lint -km $(USER) -kb $(PATH_$@) --strict

	# run build
	cd qmk_firmware; qmk compile -kb $(PATH_$@) -km $(USER)

	# copy the hex
	cd qmk_firmware; qmk flash -kb $(PATH_$@) -km $(USER)

	# cleanup symlinks
	rm -rf qmk_firmware/keyboards/$(PATH_$@)/keymaps/$(USER)
	rm -rf qmk_firmware/users/$(USER)

clean:
	rm -rf ./build/
	rm -rf qmk_firmware/keyboards/$(PATH_kyria)/keymaps/$(USER)
	rm -rf qmk_firmware/keyboards/$(PATH_lumberjack)/keymaps/$(USER)
	rm -rf qmk_firmware/users/$(USER)
