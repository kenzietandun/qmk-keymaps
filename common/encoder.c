#include QMK_KEYBOARD_H

#ifdef ENCODER_ENABLE
bool encoder_update_user(uint8_t index, bool clockwise) {
	if (clockwise) {
		switch(biton32(layer_state)) {
			case 3:
				tap_code16(KC_SLCK);
				break;
			default:
				tap_code16(KC_VOLD);
				break;
		}
	} else {
		switch(biton32(layer_state)) {
			case 3:
				tap_code16(KC_PAUS);
				break;
			default:
				tap_code16(KC_VOLU);
				break;
		}
	}
	return false;
}
#endif
