#include QMK_KEYBOARD_H

enum combo_events {
    VIM_WRITE,
    EMAIL,
    PHONE,
    DASH,
    EQUAL,
    GETSET
};

const uint16_t PROGMEM vim_write[] = {KC_Q, KC_J, COMBO_END};
const uint16_t PROGMEM email[] = {KC_Q, KC_M, COMBO_END};
const uint16_t PROGMEM phone[] = {KC_Q, KC_P, COMBO_END};
const uint16_t PROGMEM dash[] = {KC_J, KC_K, COMBO_END};
const uint16_t PROGMEM equal[] = {KC_U, KC_I, COMBO_END};
const uint16_t PROGMEM getset[] = {KC_G, KC_S, COMBO_END};

combo_t key_combos[] = {
    [VIM_WRITE] = COMBO_ACTION(vim_write),
    [EMAIL] = COMBO_ACTION(email),
    [PHONE] = COMBO_ACTION(phone),
    [DASH] = COMBO_ACTION(dash),
    [EQUAL] = COMBO_ACTION(equal),
    [GETSET] = COMBO_ACTION(getset),
};

void process_combo_event(uint16_t combo_index, bool pressed) {
    switch (combo_index) {
        case PHONE:
            if (pressed) {
                SEND_STRING("0220162936");
            }
            break;
        case EMAIL:
            if (pressed) {
                SEND_STRING("nudnateiznek@gmail.com");
            }
            break;
        case VIM_WRITE:
            if (pressed) {
                SEND_STRING(":w" SS_TAP(X_ENTER));
            }
            break;
        case DASH:
            if (pressed) {
                SEND_STRING("-");
            }
            break;
        case EQUAL:
            if (pressed) {
                SEND_STRING("=");
            }
            break;
        case GETSET:
            if (pressed) {
                SEND_STRING("{ get; set; }");
            }
            break;
    }
}