#pragma once

// keys held longer than this (in ms) registers as HOLD instead of TAP
#define TAPPING_TERM 140

// Disable auto repeating keys
#define TAPPING_FORCE_HOLD

// Prevent mod keys from registering if held less than TAPPING TERM
#define IGNORE_MOD_TAP_INTERRUPT

#define MK_3_SPEED
#define MK_C_OFFSET_1 18
#define MK_C_INTERVAL_1 16
