BOOTLOADER = dfu

OLED_ENABLE = yes
OLED_DRIVER = SSD1306   # Enables the use of OLED displays
ENCODER_ENABLE = yes       # Enables the use of one or more encoders
COMBO_ENABLE = yes

RGBLIGHT_ENABLE = no # Enable keyboard RGB underglow
LEADER_ENABLE = no        # Enable the Leader Key feature
MOUSEKEY_ENABLE = no
WPM_ENABLE = no
COMMAND_ENABLE = no
UNICODE_ENABLE = no
AUDIO_ENABLE = no
DEBOUNCE_TYPE = sym_defer_g

EXTRAFLAGS += -flto
SRC += my_kyria.c
SRC += common/combos.c
SRC += common/encoder.c
