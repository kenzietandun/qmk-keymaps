/* Copyright 2019 Thomas Baart <thomas@splitkb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#define NO_USB_STARTUP_CHECK
#define USB_SUSPEND_WAKEUP_DELAY 500

// EC11K encoders have a different resolution than other EC11 encoders.
// When using the default resolution of 4, if you notice your encoder skipping
// every other tick, lower the resolution to 2.
#define ENCODER_RESOLUTION 2

// tapping term defines for how long in milliseconds you need to hold a key before the tap becomes a hold.
#define TAPPING_TERM 120
#define TAPPING_TERM_PER_KEY

// Disable auto repeating keys
#define TAPPING_FORCE_HOLD

// Prevent mod keys from registering if held less than TAPPING TERM
#define IGNORE_MOD_TAP_INTERRUPT

// Allows media codes to properly register in macros and rotary encoder code
#define TAP_CODE_DELAY 10

#define MASTER_RIGHT

#define DISABLE_LEADER

#define DEBOUNCE 5
